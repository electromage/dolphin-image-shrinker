# Dolphin Image Shrinker

KDE Dolphin Service Menu that shrinks an image to 1024px on the long edge.

## Dependencies

* KDE+Dolphin File Manager
* convert command, included with Imagemagick.

## References

https://develop.kde.org/docs/dolphin/service-menus/

https://askubuntu.com/questions/719262/how-do-i-add-custom-items-to-the-context-menu-in-dolphin-in-kde-5

