#!/bin/bash

FILEPATH=$1

DIRNAME=`dirname $FILEPATH`
BASENAME=`basename $FILEPATH`

convert $FILEPATH -resize 1024x1024\> $DIRNAME/th_$BASENAME