#!/bin/bash

if [[ $1 == "uninstall" ]]
then
    echo "Uninstalling..."
    rm $HOME/.local/bin/DolphinImageShrinker.sh
    rm $HOME/.local/share/kservices5/ServiceMenus/ResizeImage.desktop
    kbuildsycoca5 2> /dev/null
    echo "Uninstall complete."
    exit
fi

# Check for Imagemagick "convert"
if ! command -v convert &> /dev/null
then
    echo "COMMAND could not be found"
    exit
fi

# Install script
mkdir -p $HOME/.local/bin/
install --mode=755 DolphinImageShrinker.sh $HOME/.local/bin/DolphinImageShrinker.sh

# Install .desktop file
mkdir -p $HOME/.local/share/kservices5/ServiceMenus/
cp ResizeImage.desktop $HOME/.local/share/kservices5/ServiceMenus/ResizeImage.desktop

# Rebuild the system configuration cache
kbuildsycoca5 2> /dev/null